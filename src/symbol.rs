use super::source::Span;
use std::{
    cell::Cell,
    cmp::Ordering,
    fmt,
    hash::{Hash, Hasher},
    rc::Rc,
};

type Count = u128;

#[derive(Clone)]
pub struct SymbolMaker {
    inner: Rc<Inner>,
}

impl SymbolMaker {
    pub fn new() -> Self {
        Self { inner: Rc::new(Inner::default()) }
    }

    pub fn make(&self, span: Option<Span>) -> Symbol {
        let mut this = self.clone();

        loop {
            match this.inner.next.take() {
                Some(next) => {
                    this.inner.next.set(Some(next.clone()));
                    this = next;
                },

                None => {
                    let count = this.inner.count.get();
                    if count == Count::max_value() {
                        let new = Self::new();
                        this.inner.next.set(Some(new));
                    } else {
                        this.inner.count.set(count + 1);
                    }
                    break Symbol { count, span, maker: this };
                },
            };
        }
    }
}

impl Default for SymbolMaker {
    fn default() -> Self {
        Self::new()
    }
}

impl PartialEq for SymbolMaker {
    fn eq(&self, other: &Self) -> bool {
        (&*self.inner as *const Inner) == (&*other.inner as *const _)
    }
}

impl Eq for SymbolMaker {}

impl PartialOrd for SymbolMaker {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SymbolMaker {
    fn cmp(&self, other: &Self) -> Ordering {
        (&*self.inner as *const Inner).cmp(&(&*other.inner as *const _))
    }
}

impl Hash for SymbolMaker {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        (&*self.inner as *const Inner).hash(hasher)
    }
}

impl fmt::Debug for SymbolMaker {
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmtr,
            "SymbolMaker {} {:?} -> count: {}, next: {:?} {}",
            '{',
            &*self.inner as *const Inner,
            self.inner.count.get(),
            {
                let next = self.inner.next.take();
                self.inner.next.set(next.clone());
                next
            },
            '}'
        )
    }
}

#[derive(Default)]
struct Inner {
    count: Cell<u128>,
    next: Cell<Option<SymbolMaker>>,
}

#[derive(Debug, Clone)]
pub struct Symbol {
    maker: SymbolMaker,
    count: Count,
    span: Option<Span>,
}

impl Symbol {
    pub fn maker(&self) -> &SymbolMaker {
        &self.maker
    }

    pub fn span(&self) -> Option<&Span> {
        self.span.as_ref()
    }

    pub fn clone_with_span(&self, span: Option<Span>) -> Self {
        Self { maker: self.maker.clone(), count: self.count, span }
    }

    pub fn new_with_span(&self) -> Self {
        let span = self.span.clone();
        self.maker.make(span)
    }
}

impl PartialEq for Symbol {
    fn eq(&self, other: &Self) -> bool {
        self.maker == other.maker && self.count == other.count
    }
}

impl Eq for Symbol {}

impl PartialOrd for Symbol {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Symbol {
    fn cmp(&self, other: &Self) -> Ordering {
        self.maker.cmp(&other.maker).then_with(|| self.count.cmp(&other.count))
    }
}

impl Hash for Symbol {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        self.maker.hash(hasher);
        self.count.hash(hasher);
    }
}
