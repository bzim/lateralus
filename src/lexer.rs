use super::{
    error::{Diagnostic, ErrorKind},
    source::Reader,
    token::{Token, TokenKind, TokenPattern},
};

#[derive(Debug, Clone)]
pub struct Lexer {
    toks: Vec<Result<Token, ()>>,
    pos: usize,
    reader: Reader,
}

impl Lexer {
    pub fn new(reader: Reader, errs: &mut Diagnostic) -> Self {
        let mut this = Self { toks: Vec::with_capacity(1), pos: 0, reader };
        let res = this.read(errs);
        this.toks.push(res);
        this
    }

    pub fn reader(&self) -> &Reader {
        &self.reader
    }

    pub fn reader_mut(&mut self) -> &mut Reader {
        &mut self.reader
    }

    pub fn is_eof(&self) -> bool {
        self.curr().ok().map_or(false, |tok| tok.kind == TokenKind::Eof)
    }

    pub fn curr(&self) -> Result<Token, ()> {
        self.toks[self.pos].clone()
    }

    pub fn next(&mut self, errs: &mut Diagnostic) -> bool {
        self.advance(1, errs) == 1
    }

    pub fn prev(&mut self) -> bool {
        self.rollback(1) == 1
    }

    pub fn advance(&mut self, count: usize, errs: &mut Diagnostic) -> usize {
        let mut advanced = count.min(self.toks.len() - self.pos - 1);
        self.pos += advanced;

        while self
            .curr()
            .ok()
            .filter(|tok| tok.kind != TokenKind::Eof && advanced < count)
            .is_some()
        {
            let tok = self.read(errs);
            self.toks.push(tok);
            self.pos += 1;
            advanced += 1;
        }
        advanced
    }

    pub fn rollback(&mut self, count: usize) -> usize {
        let rolled = count.min(self.pos);
        self.pos -= rolled;
        rolled
    }

    pub fn check<P>(&self, pat: P, errs: &mut Diagnostic) -> Result<Token, ()>
    where
        P: TokenPattern,
    {
        let tok = self.curr()?;

        if pat.test(&tok) {
            Ok(tok)
        } else {
            let err = ErrorKind::expected(pat, tok);
            Err(errs.raise(err))
        }
    }

    pub fn expect<P>(
        &mut self,
        pat: P,
        errs: &mut Diagnostic,
    ) -> Result<Token, ()>
    where
        P: TokenPattern,
    {
        let tok = self.check(pat, errs)?;
        self.next(errs);
        Ok(tok)
    }

    fn read(&mut self, errs: &mut Diagnostic) -> Result<Token, ()> {
        self.skip_discardable();

        if self.is_ident_start() {
            self.read_ident(errs)
        } else if self.is_typing() {
            self.read_typing(errs)
        } else if self.is_def() {
            self.read_def(errs)
        } else if self.is_open_paren() {
            self.read_open_paren(errs)
        } else if self.is_close_paren() {
            self.read_close_paren(errs)
        } else {
            self.read_eof(errs)
        }
    }

    fn skip_discardable(&mut self) {
        self.skip_whitespace();
        while self.skip_comment() && self.skip_whitespace() {}
    }

    fn skip_whitespace(&mut self) -> bool {
        let mut skipped = false;

        while self.is_whitespace() {
            self.reader.next();
            skipped = true;
        }

        skipped
    }

    fn skip_comment(&mut self) -> bool {
        if self.skip_line_comment_start() {
            while self.reader.curr().filter(|&s| s != "\n").is_some() {
                self.reader.next();
            }

            true
        } else {
            false
        }
    }

    fn skip_line_comment_start(&mut self) -> bool {
        if self.reader.curr() == Some("#") {
            self.reader.next();
            true
        } else {
            false
        }
    }

    fn is_whitespace(&self) -> bool {
        self.reader
            .curr()
            .map_or(false, |ch| ch.contains(|ch| char::is_whitespace(ch)))
    }

    fn is_ident_start(&self) -> bool {
        self.reader.curr().map_or(false, |ch| {
            ch.len() == 1
                && (ch >= "a" && ch <= "z"
                    || ch >= "A" && ch <= "Z"
                    || ch == "_")
        })
    }

    fn is_ident_part(&self) -> bool {
        self.reader.curr().map_or(false, |ch| {
            ch.len() == 1
                && (ch == "_"
                    || ch >= "a" && ch <= "z"
                    || ch >= "A" && ch <= "Z"
                    || ch >= "0" && ch <= "9")
        })
    }

    fn is_def(&self) -> bool {
        self.reader.curr().map_or(false, |ch| ch == "=")
    }

    fn is_typing(&self) -> bool {
        self.reader.curr().map_or(false, |ch| ch == ":")
    }

    fn is_open_paren(&self) -> bool {
        self.reader.curr().map_or(false, |ch| ch == "(")
    }

    fn is_close_paren(&self) -> bool {
        self.reader.curr().map_or(false, |ch| ch == ")")
    }

    fn read_ident(&mut self, _errs: &mut Diagnostic) -> Result<Token, ()> {
        self.reader.mark();
        while self.is_ident_part() {
            self.reader.next();
        }

        let span = self.reader.span();
        let kind = match &*span.content() {
            "val" => TokenKind::Val,
            "impl" => TokenKind::Impl,
            _ => TokenKind::Ident,
        };

        Ok(Token { kind, span })
    }

    fn read_def(&mut self, _errs: &mut Diagnostic) -> Result<Token, ()> {
        self.reader.mark();
        self.reader.next();
        Ok(Token { kind: TokenKind::Def, span: self.reader.span() })
    }

    fn read_typing(&mut self, _errs: &mut Diagnostic) -> Result<Token, ()> {
        self.reader.mark();
        self.reader.next();
        Ok(Token { kind: TokenKind::Typing, span: self.reader.span() })
    }

    fn read_open_paren(&mut self, _errs: &mut Diagnostic) -> Result<Token, ()> {
        self.reader.mark();
        self.reader.next();
        Ok(Token { kind: TokenKind::OpenParen, span: self.reader.span() })
    }

    fn read_close_paren(
        &mut self,
        _errs: &mut Diagnostic,
    ) -> Result<Token, ()> {
        self.reader.mark();
        self.reader.next();
        Ok(Token { kind: TokenKind::CloseParen, span: self.reader.span() })
    }

    fn read_eof(&mut self, errs: &mut Diagnostic) -> Result<Token, ()> {
        self.reader.mark();
        if self.reader.next() {
            Err(errs.raise(ErrorKind::BadChar(self.reader.span())))
        } else {
            Ok(Token { kind: TokenKind::Eof, span: self.reader.span() })
        }
    }
}
