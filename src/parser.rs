use super::{
    ast::{Ast, Expr, ExprKind, Impl, PartialType, Stmt, Val},
    error::Diagnostic,
    lexer::Lexer,
    source::Src,
    token::{TokenKind, TokenPattern},
};

pub fn parse<S0, S1>(
    name: S0,
    src: S1,
    errs: &mut Diagnostic,
) -> Result<Ast<PartialType>, ()>
where
    S0: Into<Box<str>>,
    S1: Into<Box<str>>,
{
    let reader = Src::new(name, src).reader();
    let lexer = Lexer::new(reader, errs);
    let mut parser = Parser::new(lexer);
    parser.parse(TokenKind::Eof, errs)
}

pub struct Parser {
    lexer: Lexer,
}

impl Parser {
    pub fn new(lexer: Lexer) -> Self {
        Self { lexer }
    }

    pub fn lexer(&self) -> &Lexer {
        &self.lexer
    }

    pub fn lexer_mut(&mut self) -> &mut Lexer {
        &mut self.lexer
    }

    pub fn into_lexer(self) -> Lexer {
        self.lexer
    }

    pub fn is_eof(&self) -> bool {
        self.lexer.is_eof()
    }

    pub fn parse_val<P>(
        &mut self,
        end: P,
        errs: &mut Diagnostic,
    ) -> Result<Val<PartialType>, ()>
    where
        P: TokenPattern,
    {
        self.lexer.expect(TokenKind::Val, errs)?;
        let lhs = self.lexer.expect(TokenKind::Ident, errs)?;
        self.lexer.expect(TokenKind::Typing, errs)?;
        let typ = self.parse_expr(&end, errs)?;

        let pat: &[&dyn TokenPattern] = &[&TokenKind::Def, &end];
        let maybe_def = self.lexer.check(pat, errs)?;

        let rhs = match maybe_def.kind {
            TokenKind::Def => {
                self.lexer.next(errs);
                Some(self.parse_expr(&end, errs)?)
            },
            _ => None,
        };

        Ok(Val { lhs: lhs.span.content(), typ, rhs })
    }

    pub fn parse_impl<P>(
        &mut self,
        end: P,
        errs: &mut Diagnostic,
    ) -> Result<Impl<PartialType>, ()>
    where
        P: TokenPattern,
    {
        self.lexer.expect(TokenKind::Impl, errs)?;
        let lhs = self.lexer.expect(TokenKind::Ident, errs)?;
        self.lexer.expect(TokenKind::Def, errs)?;
        let rhs = self.parse_expr(end, errs)?;

        Ok(Impl { lhs: lhs.span.content(), rhs })
    }

    pub fn parse_expr<P>(
        &mut self,
        end: P,
        errs: &mut Diagnostic,
    ) -> Result<Expr<PartialType>, ()>
    where
        P: TokenPattern,
    {
        let pat: &[_] = &[TokenKind::OpenParen, TokenKind::Ident];
        let tok = self.lexer.expect(pat, errs)?;

        Ok(match tok.kind {
            TokenKind::Ident => {
                Expr::new(ExprKind::Var(tok.span.content()), None)
            },

            TokenKind::OpenParen => {
                if self.lexer.curr()?.kind == TokenKind::CloseParen {
                    self.lexer.next(errs);
                    Expr::new(ExprKind::Unit, None)
                } else {
                    let pat: &[&dyn TokenPattern] =
                        &[&TokenKind::CloseParen, &end];
                    let sub = self.parse_expr(pat, errs)?;
                    self.lexer.expect(TokenKind::CloseParen, errs)?;
                    sub
                }
            },

            _ => unreachable!(),
        })
    }

    pub fn parse_stmt<P>(
        &mut self,
        end: P,
        errs: &mut Diagnostic,
    ) -> Result<Stmt<PartialType>, ()>
    where
        P: TokenPattern,
    {
        let pat: &[_] = &[TokenKind::Val, TokenKind::Impl];
        let tok = self.lexer.check(pat, errs)?;

        let pat: &[&dyn TokenPattern] = &[&pat, &end];

        match tok.kind {
            TokenKind::Val => {
                let val = self.parse_val(pat, errs)?;

                Ok(Stmt::Val(val))
            },

            TokenKind::Impl => {
                let imp = self.parse_impl(pat, errs)?;

                Ok(Stmt::Impl(imp))
            },

            _ => unreachable!(),
        }
    }

    pub fn parse<P>(
        &mut self,
        end: P,
        errs: &mut Diagnostic,
    ) -> Result<Ast<PartialType>, ()>
    where
        P: TokenPattern,
    {
        let mut ast = Ast { stmts: Vec::new() };

        while !end.test(&self.lexer.curr()?) {
            ast.stmts.push(self.parse_stmt(&end, errs)?);
        }

        Ok(ast)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn orphan_val() {
        let mut errs = Diagnostic::new();
        let res = parse("<test>", "val unit : ()", &mut errs);
        eprintln!("{}", errs);
        let ast = res.unwrap();
        assert_eq!(ast.stmts.len(), 1);
        match &ast.stmts[0] {
            Stmt::Val(val) => {
                assert_eq!(&val.lhs, "unit");
                assert_eq!(val.typ.kind, ExprKind::Unit);
                assert_eq!(val.rhs, None);
            },

            _ => unreachable!(),
        }
    }

    #[test]
    fn just_comments() {
        let mut errs = Diagnostic::new();
        let res =
            parse("<test>", "# testing 1 2 3\n\n# tested again", &mut errs);
        eprintln!("{}", errs);
        let ast = res.unwrap();
        assert_eq!(ast.stmts.len(), 0);
    }

    #[test]
    fn error() {
        let mut errs = Diagnostic::new();
        let res = parse(
            "<test>",
            "# testing 1 2 3\n val val val\n# tested again",
            &mut errs,
        );
        eprintln!("{}", errs);
        res.unwrap_err();
    }

    #[test]
    fn everything() {
        let mut errs = Diagnostic::new();
        let res = parse(
            "syntax.lat",
            include_str!("../examples/syntax.lat"),
            &mut errs,
        );
        eprintln!("{}", errs);
        let ast = res.unwrap();
        assert_eq!(ast.stmts.len(), 5);

        match &ast.stmts[0] {
            Stmt::Val(val) => {
                assert_eq!(&val.lhs, "unit");
                assert_eq!(val.typ.kind, ExprKind::Unit);
                assert_eq!(val.rhs, None);
            },

            _ => unreachable!(),
        }

        match &ast.stmts[1] {
            Stmt::Impl(imp) => {
                assert_eq!(&imp.lhs, "unit");
                assert_eq!(imp.rhs.kind, ExprKind::Unit);
            },

            _ => unreachable!(),
        }

        match &ast.stmts[2] {
            Stmt::Val(val) => {
                assert_eq!(&val.lhs, "unit2");
                assert_eq!(val.typ.kind, ExprKind::Unit);
                assert_eq!(val.rhs.as_ref().unwrap().kind, ExprKind::Unit);
            },

            _ => unreachable!(),
        }

        match &ast.stmts[3] {
            Stmt::Val(val) => {
                assert_eq!(&val.lhs, "orphan");
                assert_eq!(val.typ.kind, ExprKind::Unit);
                assert_eq!(val.rhs, None);
            },

            _ => unreachable!(),
        }

        match &ast.stmts[4] {
            Stmt::Val(val) => {
                assert_eq!(&val.lhs, "with_ident");
                assert_eq!(val.typ.kind, ExprKind::Unit);

                match &val.rhs.as_ref().unwrap().kind {
                    ExprKind::Var(ident) => assert_eq!(&*ident, "unit"),
                    _ => unreachable!(),
                }
            },

            _ => unreachable!(),
        }
    }
}
