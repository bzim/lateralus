use super::{
    bind_map::BindMap,
    error::{Diagnostic, ErrorKind},
    source::SpanContent,
};
use std::fmt;

pub type PartialType = Option<Type>;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Type {
    Unit,
    Type,
}

impl Type {
    pub fn check() {}
}

impl fmt::Display for Type {
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Type::Unit => fmtr.write_str("()"),
            Type::Type => fmtr.write_str("{{type}}"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Ast<T> {
    pub stmts: Vec<Stmt<T>>,
}

impl Ast<PartialType> {
    pub fn type_check(self, errs: &mut Diagnostic) -> Result<Ast<Type>, ()> {
        let mut ast = Ast { stmts: Vec::new() };
        let mut bind_map = BindMap::new();

        for stmt in self.stmts {
            let stmt = stmt.type_check(&mut bind_map, errs)?;
            ast.stmts.push(stmt);
        }

        Ok(ast)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Stmt<T> {
    Val(Val<T>),
    Impl(Impl<T>),
}

impl Stmt<PartialType> {
    fn type_check(
        self,
        map: &mut BindMap<SpanContent, PartialType>,
        errs: &mut Diagnostic,
    ) -> Result<Stmt<Type>, ()> {
        match self {
            Stmt::Val(val) => val.type_check(map, errs).map(Stmt::Val),
            Stmt::Impl(imp) => imp.type_check(map, errs).map(Stmt::Impl),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Val<T> {
    pub lhs: SpanContent,
    pub typ: Expr<T>,
    pub rhs: Option<Expr<T>>,
}

impl Val<PartialType> {
    fn type_check(
        self,
        map: &mut BindMap<SpanContent, PartialType>,
        errs: &mut Diagnostic,
    ) -> Result<Val<Type>, ()> {
        let Self { lhs, typ, rhs } = self;

        let typ = typ.type_check(map, errs, Some(Type::Type));

        let rhs = match rhs {
            Some(expr) => {
                expr.type_check(map, errs, Some(Type::Unit)).map(Some)
            },
            None => Ok(None),
        };

        map.def(lhs.clone(), Some(Type::Unit));

        let typ = typ?;
        let rhs = rhs?;

        Ok(Val { lhs, typ, rhs })
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Impl<T> {
    pub lhs: SpanContent,
    pub rhs: Expr<T>,
}

impl Impl<PartialType> {
    fn type_check(
        self,
        map: &mut BindMap<SpanContent, PartialType>,
        errs: &mut Diagnostic,
    ) -> Result<Impl<Type>, ()> {
        let Self { lhs, rhs } = self;
        let typ = map.get(&lhs).map(|x| *x).ok_or_else(|| {
            let lhs = lhs.clone();
            errs.raise(ErrorKind::Undefined(lhs))
        });

        let typ_kind = typ.ok().and_then(|x| x);

        let rhs = rhs.type_check(map, errs, typ_kind)?;
        typ?;

        Ok(Impl { lhs, rhs })
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Expr<T> {
    pub kind: ExprKind,
    pub typ: T,
    _priv: (),
}

impl Expr<PartialType> {
    pub fn new(kind: ExprKind, typ: PartialType) -> Self {
        Self { kind, typ, _priv: () }
    }

    fn type_check(
        self,
        map: &mut BindMap<SpanContent, PartialType>,
        errs: &mut Diagnostic,
        prefer: PartialType,
    ) -> Result<Expr<Type>, ()> {
        let Self { kind, typ, .. } = self;

        let typ = typ.or(prefer);

        match kind {
            ExprKind::Unit => {
                Ok(Expr { kind, typ: typ.unwrap_or(Type::Unit), _priv: () })
            },

            ExprKind::Var(ident) => {
                let found = map.get(&ident).map(|x| *x).ok_or_else(|| {
                    let ident = ident.clone();
                    errs.raise(ErrorKind::Undefined(ident))
                })?;

                match (typ, found) {
                    (Some(t1), Some(t2)) if t1 != t2 => {
                        Err(errs.raise(ErrorKind::MisType(t1, t2)))
                    },

                    (Some(typ), _) | (None, Some(typ)) => {
                        Ok(Expr { typ, kind: ExprKind::Var(ident), _priv: () })
                    },

                    _ => Err(()),
                }
            },
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ExprKind {
    Unit,
    Var(SpanContent),
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::parser::parse;

    #[test]
    fn error() {
        let mut errs = Diagnostic::new();
        let parse_res = parse(
            "syntax.lat",
            "val unit : () = ()\nval unit2 : unit = ()",
            &mut errs,
        );
        let res = parse_res.and_then(|ast| ast.type_check(&mut errs));
        eprintln!("{}", errs);
        res.unwrap_err();
    }

    #[test]
    fn example_checks() {
        let mut errs = Diagnostic::new();
        let parse_res = parse(
            "syntax.lat",
            include_str!("../examples/syntax.lat"),
            &mut errs,
        );
        let res = parse_res.and_then(|ast| ast.type_check(&mut errs));
        eprintln!("{}", errs);
        res.unwrap();
    }
}
