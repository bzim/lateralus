use std::{
    borrow::Borrow,
    collections::hash_map::{Entry, HashMap},
    hash::Hash,
};

#[derive(Debug, Clone)]
pub struct BindMap<K, V>
where
    K: Hash + Eq,
{
    bindings: HashMap<K, Vec<V>>,
}

impl<K, V> BindMap<K, V>
where
    K: Hash + Eq,
{
    pub fn new() -> Self {
        Self { bindings: HashMap::new() }
    }

    pub fn def(&mut self, key: K, val: V) {
        match self.bindings.entry(key) {
            Entry::Vacant(vac) => {
                vac.insert(vec![val]);
            },

            Entry::Occupied(mut occ) => occ.get_mut().push(val),
        }
    }

    pub fn undef<Q>(&mut self, key: &Q) -> Option<V>
    where
        Q: Hash + Eq + ?Sized,
        K: Borrow<Q>,
    {
        let (elem, len) = {
            let vec = self.bindings.get_mut(key)?;
            (vec.pop(), vec.len())
        };

        if len == 0 {
            self.bindings.remove(key);
        }

        elem
    }

    pub fn get<Q>(&self, key: &Q) -> Option<&V>
    where
        Q: Hash + Eq + ?Sized,
        K: Borrow<Q>,
    {
        self.bindings.get(key).map(|vec| &vec[vec.len() - 1])
    }

    pub fn get_mut<Q>(&mut self, key: &Q) -> Option<&mut V>
    where
        Q: Hash + Eq + ?Sized,
        K: Borrow<Q>,
    {
        self.bindings.get_mut(key).map(|vec| {
            let idx = vec.len() - 1;
            &mut vec[idx]
        })
    }
}

impl<K, V> Default for BindMap<K, V>
where
    K: Hash + Eq,
{
    fn default() -> Self {
        Self::new()
    }
}
